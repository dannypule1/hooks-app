import React, { useState } from 'react';
// import logo from './logo.svg';
import './App.css';

const App = () => {
  const [num, setNum] = useState(43);

  return (
    <div>
      <h1>{window.AppConfig.API_URL}</h1>
      <h1>{num}</h1>
      <button onClick={() => setNum(num + 1)}>+</button>
      <button onClick={() => setNum(num - 1)}>-</button>
    </div>
  );
}

export default App;
